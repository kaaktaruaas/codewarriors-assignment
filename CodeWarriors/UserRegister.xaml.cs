﻿using CodeWarriors.Common;
using CodeWarriors.Model;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace CodeWarriors
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class UserRegister : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        public UserRegister()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion


        #region myCodeStarts

        private IMobileServiceTable<CodeWarriorsKBS> CodeWarriorsKBS_Table = App.MobileService.GetTable<CodeWarriorsKBS>();

        string us_name;


        private void fullNameBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            fullNameBlock.Margin = new Thickness(0, -36, 0, 0);

            fullNameBox.Visibility = Windows.UI.Xaml.Visibility.Visible;

            fullNameBox.Focus(Windows.UI.Xaml.FocusState.Pointer);
        }

        private void fullNameBox_LostFocus(object sender, RoutedEventArgs e)
        {
            fullNameBlock.Margin = new Thickness(0, 0, 0, 0);
        }



        private void userNameBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            userNameBlock.Margin = new Thickness(0, -36, 0, 0);

            userNameBox.Visibility = Windows.UI.Xaml.Visibility.Visible;

            userNameBox.Focus(Windows.UI.Xaml.FocusState.Pointer);
        }

        private void userNameBox_LostFocus(object sender, RoutedEventArgs e)
        {
            userNameBlock.Margin = new Thickness(0, 0, 0, 0);
        }


        private void userPhoneBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            userPhoneBlock.Margin = new Thickness(0, -36, 0, 0);

            userPhoneBox.Visibility = Windows.UI.Xaml.Visibility.Visible;

            userPhoneBox.Focus(Windows.UI.Xaml.FocusState.Pointer);

        }

        private void userPhoneBox_LostFocus(object sender, RoutedEventArgs e)
        {
            userPhoneBlock.Margin = new Thickness(0, 0, 0, 0);
        }

        private void userEmailBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            userEmailBlock.Margin = new Thickness(0, -36, 0, 0);

            userEmailBox.Visibility = Windows.UI.Xaml.Visibility.Visible;

            userEmailBox.Focus(Windows.UI.Xaml.FocusState.Pointer);

        }

        private void userEmailBox_LostFocus(object sender, RoutedEventArgs e)
        {
            userEmailBlock.Margin = new Thickness(0, 0, 0, 0);

        }

        private void userPasswordBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            userPasswordBlock.Margin = new Thickness(0, -36, 0, 0);

            userPasswordBox.Visibility = Windows.UI.Xaml.Visibility.Visible;

            userPasswordBox.Focus(Windows.UI.Xaml.FocusState.Pointer);
        }

        private void userPasswordBox_LostFocus(object sender, RoutedEventArgs e)
        {
            userPasswordBlock.Margin = new Thickness(0, 0, 0, 0);

        }



        private void RegisterOkayButton_Tapped(object sender, TappedRoutedEventArgs e)
        {

            string genderdeterminestring = "";


            if (MrGender.IsSelected == true)
            {
                genderdeterminestring = "male";
            }

            if (MsGender.IsSelected == true)
            {
                genderdeterminestring = "female";
            }

            string full_name = fullNameBox.Text;

            us_name = userNameBox.Text;

            string us_phone = userPhoneBox.Text;

            string us_email = userEmailBox.Text;

            string us_password = userPasswordBox.Password;

            string us_HashPassword = MD5CryptoServiceProvider.GetMd5String(us_password);

            bool us_terms = false;

            if (TermsAgree.IsChecked == true)
            {
                us_terms = true;
            }

            else
            {
                us_terms = false;
            }

            if (us_terms == true)
            {
                var usrinfo = new CodeWarriorsKBS()
                {

                    UserGender = genderdeterminestring,

                    FullName = full_name,

                    UserName = us_name,

                    Phone = us_phone,

                    Email = us_email,

                    Password = us_HashPassword,

                };

                RegistrationSuccessMethod(usrinfo);
            }

            else
            {
                PleaseTermsAgree();
            }
        }

        private async void PleaseTermsAgree()
        {
            MessageDialog msg = new MessageDialog("Please agree to the terms and conditions!");

            await msg.ShowAsync();

        }

        private async void RegistrationSuccessMethod(CodeWarriorsKBS usrinfo)
        {
            try
            {
                await CodeWarriorsKBS_Table.InsertAsync(usrinfo);

                Congrats();

                Frame.Navigate(typeof(Dashboard));
            }
            catch(Exception)
            {
                SorryFailMethod();

            }

        }

        private async void Congrats()
        {
            MessageDialog msg = new MessageDialog("Your registration has successfully been completed");

            await msg.ShowAsync();

            PageNavigationAfterSuccess();
        }

        private void PageNavigationAfterSuccess()
        {
            Frame.Navigate(typeof(Dashboard), us_name);
        }

        private async void SorryFailMethod()
        {
            MessageDialog msg = new MessageDialog("Sorry! User Registration failed! UserName not unique or see your internet Connection! Thank you.");

            await msg.ShowAsync();
        }




        #endregion
    }
}
