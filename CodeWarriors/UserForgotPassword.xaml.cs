﻿using CodeWarriors.Common;
using CodeWarriors.Model;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Email;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace CodeWarriors
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class UserForgotPassword : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        public UserForgotPassword()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion


        #region

        private MobileServiceCollection<CodeWarriorsKBS, CodeWarriorsKBS> u_info;

        private IMobileServiceTable<CodeWarriorsKBS> CodeWarriorsKBS_Table = App.MobileService.GetTable<CodeWarriorsKBS>();


        private void userNameForgotBlock_Tapped(object sender, TappedRoutedEventArgs e)
        {
            userNameForgotBlock.Margin = new Thickness(0, -36, 0, 0);

            userNameForgotBox.Visibility = Windows.UI.Xaml.Visibility.Visible;

            userNameForgotBox.Focus(Windows.UI.Xaml.FocusState.Pointer);

        }

        private void userNameForgotBox_LostFocus(object sender, RoutedEventArgs e)
        {
            userNameForgotBlock.Margin = new Thickness(0, 0, 0, 0);
        }


        private async void LoginForgotButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            string UserNameForget = userNameForgotBox.Text;

            await UserLoginValidate(UserNameForget);
        }



        private async Task UserLoginValidate(string UserNameLogin)
        {
            MobileServiceInvalidOperationException exception = null;

            try
            {
                // This code refreshes the entries in the list view by querying the TodoItems table.
                // The query excludes completed TodoItems
                u_info = await CodeWarriorsKBS_Table
                    .Where(user_info => (user_info.UserName == UserNameLogin))
                    .ToCollectionAsync();

                int len = u_info.Count;

            }

            catch (MobileServiceInvalidOperationException e)
            {
                exception = e;
            }

            if (exception != null)
            {
                await new MessageDialog(exception.Message, "Error loading items").ShowAsync();
            }
            else
            {
                try
                {
                    if (u_info[0].UserName == UserNameLogin)
                    {
                        string userEmail = u_info[0].Email;

                        string newUserPassword = GetPassword();

                        string newHashPassword = MD5CryptoServiceProvider.GetMd5String(newUserPassword);


                        var updatedusrinfo = new CodeWarriorsKBS()
                        {
                            Password = newHashPassword,
                        };

                        await CodeWarriorsKBS_Table.UpdateAsync(updatedusrinfo);

                        Congrats();

                    }

                    else
                    {
                        Unsuccessful();
                    }

                }

                catch (Exception)
                {
                    InvalidUserName();
                }
            }

        }



        private int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        private string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        public string GetPassword()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(RandomString(4, true));
            builder.Append(RandomNumber(1000, 9999));
            builder.Append(RandomString(2, false));
            return builder.ToString();
        }

        private async void InvalidUserName()
        {
            MessageDialog msg = new MessageDialog("Invalid Username! Please try again.");

            await msg.ShowAsync();
        }


        private async void Congrats()
        {
            MessageDialog msg = new MessageDialog("An email has been sent to your Email Account with new Password!");

            await msg.ShowAsync();
        }

        private async void Unsuccessful()
        {
            MessageDialog msg = new MessageDialog("Please check your internet connection and try again!");

            await msg.ShowAsync();
        }





        #endregion

    }
}
