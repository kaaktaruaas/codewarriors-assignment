﻿

#pragma checksum "G:\DragonHunter\KAAKTARUAA\CodeWarriors\Project\CodeWarriors\CodeWarriors\UserRegister.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "E0EE12E9D16A726D5F7970409C27DCF1"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CodeWarriors
{
    partial class UserRegister : global::Windows.UI.Xaml.Controls.Page
    {
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Grid LayoutRoot; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Grid ContentRoot; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Grid SignUpPage; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.ComboBox PassangerPlaceHolder; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.CheckBox TermsAgree; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Shapes.Rectangle RegisterOkayButton; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.TextBlock RegBlock; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.TextBlock userPasswordBlock; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.PasswordBox userPasswordBox; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.TextBlock userEmailBlock; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.TextBox userEmailBox; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.TextBlock userPhoneBlock; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.TextBox userPhoneBox; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.TextBlock userNameBlock; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.TextBox userNameBox; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.TextBlock fullNameBlock; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.TextBox fullNameBox; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.ComboBoxItem MrGender; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.ComboBoxItem MsGender; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private bool _contentLoaded;

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent()
        {
            if (_contentLoaded)
                return;

            _contentLoaded = true;
            global::Windows.UI.Xaml.Application.LoadComponent(this, new global::System.Uri("ms-appx:///UserRegister.xaml"), global::Windows.UI.Xaml.Controls.Primitives.ComponentResourceLocation.Application);
 
            LayoutRoot = (global::Windows.UI.Xaml.Controls.Grid)this.FindName("LayoutRoot");
            ContentRoot = (global::Windows.UI.Xaml.Controls.Grid)this.FindName("ContentRoot");
            SignUpPage = (global::Windows.UI.Xaml.Controls.Grid)this.FindName("SignUpPage");
            PassangerPlaceHolder = (global::Windows.UI.Xaml.Controls.ComboBox)this.FindName("PassangerPlaceHolder");
            TermsAgree = (global::Windows.UI.Xaml.Controls.CheckBox)this.FindName("TermsAgree");
            RegisterOkayButton = (global::Windows.UI.Xaml.Shapes.Rectangle)this.FindName("RegisterOkayButton");
            RegBlock = (global::Windows.UI.Xaml.Controls.TextBlock)this.FindName("RegBlock");
            userPasswordBlock = (global::Windows.UI.Xaml.Controls.TextBlock)this.FindName("userPasswordBlock");
            userPasswordBox = (global::Windows.UI.Xaml.Controls.PasswordBox)this.FindName("userPasswordBox");
            userEmailBlock = (global::Windows.UI.Xaml.Controls.TextBlock)this.FindName("userEmailBlock");
            userEmailBox = (global::Windows.UI.Xaml.Controls.TextBox)this.FindName("userEmailBox");
            userPhoneBlock = (global::Windows.UI.Xaml.Controls.TextBlock)this.FindName("userPhoneBlock");
            userPhoneBox = (global::Windows.UI.Xaml.Controls.TextBox)this.FindName("userPhoneBox");
            userNameBlock = (global::Windows.UI.Xaml.Controls.TextBlock)this.FindName("userNameBlock");
            userNameBox = (global::Windows.UI.Xaml.Controls.TextBox)this.FindName("userNameBox");
            fullNameBlock = (global::Windows.UI.Xaml.Controls.TextBlock)this.FindName("fullNameBlock");
            fullNameBox = (global::Windows.UI.Xaml.Controls.TextBox)this.FindName("fullNameBox");
            MrGender = (global::Windows.UI.Xaml.Controls.ComboBoxItem)this.FindName("MrGender");
            MsGender = (global::Windows.UI.Xaml.Controls.ComboBoxItem)this.FindName("MsGender");
        }
    }
}



