﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeWarriors.Model
{
    class CodeWarriorsKBS
    {
        public string Id
        {
            get;
            set;
        }


        [JsonProperty(PropertyName = "userid")]
        public int UserId
        {
            get;
            set;
        }



        [JsonProperty(PropertyName = "fullname")]
        public string FullName
        {
            get;
            set;
        }


        [JsonProperty(PropertyName = "username")]
        public string UserName
        {
            get;
            set;
        }


        [JsonProperty(PropertyName = "password")]
        public string Password
        {
            get;
            set;
        }

        [JsonProperty(PropertyName = "usergender")]
        public string UserGender
        {
            get;
            set;
        }


        [JsonProperty(PropertyName = "phone")]
        public string Phone
        {
            get;
            set;
        }


        [JsonProperty(PropertyName = "email")]
        public string Email
        {
            get;
            set;
        }

    }
}
