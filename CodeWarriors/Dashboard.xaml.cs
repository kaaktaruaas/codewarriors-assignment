﻿using CodeWarriors.Common;
using CodeWarriors.Model;
using Microsoft.WindowsAzure.MobileServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace CodeWarriors
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Dashboard : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        public Dashboard()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        /// 

        string PassedUserName;

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);

            PassedUserName = e.Parameter as string;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion


        #region myCodeStarts


        private MobileServiceCollection<CodeWarriorsKBS, CodeWarriorsKBS> u_info;

        private IMobileServiceTable<CodeWarriorsKBS> CodeWarriorsKBS_Table = App.MobileService.GetTable<CodeWarriorsKBS>();


        private void PostAdButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            HelpingClass myParams = new HelpingClass();

            myParams.parameter1 = Loggeduserid.ToString();
            myParams.parameter2 = Loggedusername;

            Frame.Navigate(typeof(PostNewAd), myParams);
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            await RefreshLessonContents();
        }


        int Loggeduserid;
        string Loggedusername;

        private async Task RefreshLessonContents()
        {
            MobileServiceInvalidOperationException exception = null;
            try
            {
                // This code refreshes the entries in the list view by querying the table.

                u_info = await CodeWarriorsKBS_Table
                                    .Where(user_info => (user_info.UserName == PassedUserName))
                                    .ToCollectionAsync();

                int len = u_info.Count;
            }
            catch (MobileServiceInvalidOperationException e)
            {
                exception = e;
            }

            if (exception != null)
            {
                await new MessageDialog(exception.Message, "Error loading items").ShowAsync();
            }
            else
            {
                try
                {
                    if (u_info[0].UserName == PassedUserName)
                    {
                        FullNameShowing.Text = u_info[0].FullName;
                        EmailShowing.Text = u_info[0].Email;
                        PhoneShowing.Text = u_info[0].Phone;

                        string gender = u_info[0].UserGender;
                        Loggeduserid = u_info[0].UserId;
                        Loggedusername = PassedUserName;


                        if (gender.Equals("male"))
                        {
                            GenderStatusImage.Source = new BitmapImage(new Uri("ms-appx:///Images/male.png", UriKind.Absolute));
                        }

                        else
                        {
                            GenderStatusImage.Source = new BitmapImage(new Uri("ms-appx:///Images/female.png", UriKind.Absolute));
                        }

                    }

                }

                catch (Exception)
                {
                    GenderStatusImage.Source = new BitmapImage(new Uri("ms-appx:///Images/male.png", UriKind.Absolute));
                }
            }
        }


        #endregion

    }
}
